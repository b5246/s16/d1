// Repetition Control Structure

// 1) WHILE LOOP

let count = 5;
while(count !== 0){
	console.log("While: " + count);
	count--;
}
console.log("COunt 1-10");
count=1;
while(count < 11){
	console.log("count " + count);
	count++;
}



// DO-WHILE
/*let num = Number(prompt("Input a number: "));

do{
	console.log("do while(example): " + num);
	num += 1;
}while(num <10);*/

let num3 = 2;
while (num3 < 11){
	console.log("Even: " + num3);
	num3 += 2;
}

let num4 = 2;
do{
	console.log("Even (do-while): " + num4);
	num4 += 2;
}while(num4 < 11);


//FOR LOOP

for(let num5=0; num5 <=20; num5++){
	console.log("FOR-Loop: " + num5);
}

for(let num6=2, ctr=1; ctr < 6; ctr ++){
	console.log("FOR-Loop-even: " + num6);
	num6 +=2;
}


//String and array
let myString = "alex";
/*console.log(myString.length);
console.log(myString[3]);*/

for(let ctr = myString.length  , i = 0; ctr >= i; ctr--){
	console.log(myString[ctr]);
}

let myName = "DOna";
const vowel = ['a','e','i','o','u'];
for(let ctr = myName.length  , i = 0; i< ctr; i++){
	if(myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() =='o' ||
		myName[i].toLowerCase() == 'u' 
		){
		console.log("3");
	}else{
		console.log(myName[i]);
	}
}

/*for(let ctr = myName.length  , i = 0; i< ctr; i++){
	for(let x in vowel){
		if(myName[i].toLowerCase() === x){
			console.log("3");
			break;
		}
	}
}*/

for(let count =0; count<=20;count++){
	if(count %2 === 0){
		continue;
	}
	console.log("continue-break-odd-number: " + count);
	if(count >10){
		break;
	}
}

let name = 'alexander';
for(let i=0; i<name.length;i++){
	console.log(name[i]);
	if(name[i].toLowerCase() === 'a'){
		console.log("continue");
		continue;
	}
	if(name[i] === 'd'){
		break;
	}
}

